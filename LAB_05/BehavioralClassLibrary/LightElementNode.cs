﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace BehavioralClassLibrary
{
    public class LightElementNode : LightNode, IEnumerable<LightNode>
    {
        public string TagName { get; }
        public string DisplayType { get; }
        public string ClosingType { get; }
        public List<string> CssClasses { get; }
        public List<LightNode> Children { get; }

        public LightElementNode(string tagName, string displayType, string closingType, List<string>? cssClasses = null, List<LightNode>? children = null)
        {
            TagName = tagName ?? throw new ArgumentNullException(nameof(tagName));
            DisplayType = displayType ?? throw new ArgumentNullException(nameof(displayType));
            ClosingType = closingType ?? throw new ArgumentNullException(nameof(closingType));
            CssClasses = cssClasses ?? new List<string>();
            Children = children ?? new List<LightNode>();

            OnClassListApplied();
        }

        protected override void OnClassListApplied()
        {
         
            if (CssClasses.Count > 0)
            {
                Console.WriteLine($"\nClasses applied to <{TagName}>: {string.Join(", ", CssClasses)}");
            }
            else
            {
                Console.WriteLine($"\nNo classes applied to <{TagName}>");
            }
        }

        protected override void OnTextRendered()
        {
            
            Console.WriteLine($"\nText rendered for element: <{TagName}>");
        }

        public override string OuterHTML
        {
            get
            {
                OnTextRendered();

                StringBuilder html = new StringBuilder();
                html.Append($"<{TagName}");

                if (CssClasses.Count > 0)
                {
                    html.Append($" class=\"{string.Join(" ", CssClasses)}\"");
                    OnStylesApplied();
                }

                html.Append(">");

                foreach (var child in Children)
                {
                    html.Append(child.OuterHTML);
                }

                if (ClosingType == "double")
                {
                    html.Append($"</{TagName}>");
                }
                else if (ClosingType == "single")
                {
                    html.Append("/>");
                }

                return html.ToString();
            }
        }

        public override string InnerHTML
        {
            get
            {
                StringBuilder html = new StringBuilder();

                foreach (var child in Children)
                {
                    html.Append(child.OuterHTML);
                }

                return html.ToString();
            }
        }

        public IEnumerator<LightNode> GetEnumerator()
        {
            return new LightNodeIterator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private class LightNodeIterator : IEnumerator<LightNode>
        {
            private readonly LightElementNode _root;
            private readonly Stack<IEnumerator<LightNode>> _stack;
            private LightNode? _current;

            public LightNodeIterator(LightElementNode root)
            {
                _root = root ?? throw new ArgumentNullException(nameof(root));
                _stack = new Stack<IEnumerator<LightNode>>();
                _stack.Push(_root.Children.GetEnumerator());
            }

            public LightNode Current => _current ?? throw new InvalidOperationException();

            object IEnumerator.Current => Current;

            public bool MoveNext()
            {
                while (_stack.Count > 0)
                {
                    var enumerator = _stack.Peek();
                    if (enumerator.MoveNext())
                    {
                        _current = enumerator.Current;
                        if (_current is LightElementNode elementNode)
                        {
                            _stack.Push(elementNode.Children.GetEnumerator());
                        }
                        return true;
                    }
                    else
                    {
                        _stack.Pop();
                    }
                }
                _current = null;
                return false;
            }

            public void Reset()
            {
                _stack.Clear();
                _stack.Push(_root.Children.GetEnumerator());
                _current = null;
            }

            public void Dispose()
            {
                _stack.Clear();
            }
        }
    }
}
