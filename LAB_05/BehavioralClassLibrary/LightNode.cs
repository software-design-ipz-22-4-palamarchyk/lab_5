﻿namespace BehavioralClassLibrary
{
    public abstract class LightNode
    {
        public virtual string? OuterHTML { get; }
        public virtual string? InnerHTML { get; }

        protected virtual void OnCreated() { }
        protected virtual void OnInserted() { }
        protected virtual void OnRemoved() { }
        protected virtual void OnStylesApplied() { }
        protected virtual void OnClassListApplied() { }
        protected virtual void OnTextRendered() { }

        public LightNode()
        {
            OnCreated();
        }
    }
}
