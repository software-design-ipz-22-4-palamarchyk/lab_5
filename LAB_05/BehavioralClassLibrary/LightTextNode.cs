﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BehavioralClassLibrary
{
    public class LightTextNode : LightNode
    {
        public string Text { get; }

        public LightTextNode(string text)
        {
            Text = text ?? throw new ArgumentNullException(nameof(text));
            OnTextRendered();
        }

        protected override void OnTextRendered()
        {
           
            Console.WriteLine($"\nText rendered: {Text}");
        }

        public override string OuterHTML => Text;
        public override string InnerHTML => Text;
    }
}
